<?php
namespace App\Handler;

use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Created by SmartGroup.eu
 * User: Mariusz Tulikowski
 * Date: 2017-11-27
 * Time: 12:24 PM
 * E-mail: mariusz@tulikowski.eu
 * WWW: www.tulikowski.com
 */

class SessionIdleHandler
{

    protected $session;
    protected $securityToken;
    protected $router;
    protected $maxIdleTime;
    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * SessionIdleHandler constructor.
     * @param SessionInterface $session
     * @param TokenStorageInterface $securityToken
     * @param RouterInterface $router
     * @param FlashBagInterface $flashBag
     * @param int $maxIdleTime
     */
    public function __construct(
        SessionInterface $session,
        TokenStorageInterface $securityToken,
        RouterInterface $router,
        FlashBagInterface $flashBag,
        $maxIdleTime = 0
    ) {
        $this->session = $session;
        $this->securityToken = $securityToken;
        $this->router = $router;
        $this->maxIdleTime = $maxIdleTime;
        $this->flashBag = $flashBag;
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event): void
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        if ($this->maxIdleTime > 0) {
            $this->session->start();
            $lapse = time() - $this->session->getMetadataBag()->getLastUsed();

            if ($lapse > $this->maxIdleTime) {
                $this->securityToken->setToken(null);
                $this->flashBag->add('info', 'You have been logged out due to inactivity.');

                // Change the route if you are not using FOSUserBundle.
                $event->setResponse(new RedirectResponse($this->router->generate('homepage')));
            }
        }
    }

}