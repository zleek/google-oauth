<?php
namespace App\Controller;


use App\Entity\User;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;

class GoogleController extends AbstractController
{

    /**
     * @Route("/connect/google", name="connect_google_start")
     *
     * @param ClientRegistry $clientRegistry
     * @return RedirectResponse
     */
    public function connectAction(ClientRegistry $clientRegistry)
    {
        return $clientRegistry->getClient('google')->redirect(
            ['profile', 'email']
        );
    }

    /**
     * @Route("/connect/google/check", name="connect_google_check")
     *
     * @param Request $request
     * @param ClientRegistry $clientRegistry
     * @param Security $security
     * @return RedirectResponse
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry, Security $security)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        /** @var User $user */
        $user = $this->getUser();

        if (!$user->isEnabled() && $security->getToken() !== null) {
            $this->addFlash('danger', 'User not enabled');
            $security->getToken()->setAuthenticated(false);
        }
        return $this->redirectToRoute('homepage');
    }
}